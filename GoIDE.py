# Imports ↓

from PyQt6.QtCore import *
from PyQt6.QtDBus import *
from PyQt6.QtDesigner import *
from PyQt6.QtGui import *
from PyQt6.QtNetwork import *
from PyQt6.QtOpenGL import *
from PyQt6.QtOpenGLWidgets import *
from PyQt6.QtPrintSupport import *
from PyQt6.QtSql import *
from PyQt6.QtSvg import *
from PyQt6.QtSvgWidgets import *
from PyQt6.QtWidgets import *
from PyQt6.QtXml import *
from json import load, loads
from pathlib import Path
import requests
import colorsys
import sys
import os
import io

Settings = open(Path("Resources/Files/Data.json"), encoding='utf-8')
Data = Settings.read()
Dados = loads(Data)
Settings.close()

NewFiles = 0
Files = []

Language = Dados['Settings_Menu']['Language']
InterfaceFont = Dados['Settings_Menu']['Interface_Font']
InterfaceTheme = Dados['Settings_Menu']['Interface_Theme']
IT = Dados['Translations'][Language]
ICSS = Dados['Settings_Menu']['Interface'][InterfaceTheme]
GeneralCSS = Dados['Settings_Menu']['Interface'][InterfaceTheme]['GeneralCSS']


def Construir():
    class GoIDE(QMainWindow):
        def __init__(self):
            super().__init__()
            self.ProjectTree = QTreeView(self)
            self.ProjectTree.setObjectName("ProjectTree")
            self.ProjectTree.setGeometry(0, 50, 320, 1005)
            self.ProjectTree.setStyleSheet(ICSS['ProjectTree'])

            self.Files = QTabWidget(self)
            self.Files.setGeometry(self.ProjectTree.width(), 50, 1920 - self.ProjectTree.width(), 1005)
            self.Files.setStyleSheet(ICSS['Files'])
            self.Files.setTabsClosable(True)
            self.Files.setMovable(True)

            self.MenuBar = QMenuBar()
            self.MenuBar.setStyleSheet(ICSS['MenuBar'])

            self.File = QMenu(IT['File'])
            self.File.setFont(QFont(InterfaceFont))
            self.File.setStyleSheet(GeneralCSS['MenuForMenuBar'])

            self.Edit = QMenu(IT['Edit'])
            self.Edit.setFont(QFont(InterfaceFont))
            self.Edit.setStyleSheet(GeneralCSS['MenuForMenuBar'])

            self.View = QMenu(IT['View'])
            self.View.setFont(QFont(InterfaceFont))
            self.View.setStyleSheet(GeneralCSS['MenuForMenuBar'])

            self.Tools = QMenu(IT['Tools'])
            self.Tools.setFont(QFont(InterfaceFont))
            self.Tools.setStyleSheet(GeneralCSS['MenuForMenuBar'])

            self.Run = QMenu(IT['Run'])
            self.Run.setFont(QFont(InterfaceFont))
            self.Run.setStyleSheet(GeneralCSS['MenuForMenuBar'])

            self.Settings = QMenu(IT['Settings'])
            self.Settings.setFont(QFont(InterfaceFont))
            self.Settings.setStyleSheet(GeneralCSS['MenuForMenuBar'])

            self.Help = QMenu(IT['Help'])
            self.Help.setFont(QFont(InterfaceFont))
            self.Help.setStyleSheet(GeneralCSS['MenuForMenuBar'])

            self.MenuBar.addMenu(self.File)
            self.MenuBar.addMenu(self.Edit)
            self.MenuBar.addMenu(self.View)
            self.MenuBar.addMenu(self.Tools)
            self.MenuBar.addMenu(self.Run)
            self.MenuBar.addMenu(self.Settings)
            self.MenuBar.addMenu(self.Help)

            self.NewFile = QAction(IT['NewFile'])
            self.NewFile.setObjectName('NewFile')
            self.NewFile.setIcon(QIcon('Resources/Icons/NewFile.ico'))
            self.NewFile.setShortcut(QKeySequence('CTRL+N'))
            self.NewFile.triggered.connect(self.NewFile_Triggered)

            self.NewProject = QAction(IT['NewProject'])
            self.NewProject.setObjectName('NewProject')
            self.NewProject.setIcon(QIcon('Resources/Icons/NewFolder.ico'))
            self.NewProject.setShortcut(QKeySequence('CTRL+SHIFT+N'))
            self.NewProject.triggered.connect(self.NewProject_Triggered)

            self.SaveCurrentFile = QAction(IT['SaveCurrentFile'])
            self.SaveCurrentFile.setObjectName('SaveCurrentFile')
            self.SaveCurrentFile.setIcon(QIcon('Resources/Icons/Save.ico'))
            self.SaveCurrentFile.setShortcut(QKeySequence('CTRL+S'))
            self.SaveCurrentFile.triggered.connect(self.SaveCurrentFile_Triggered)

            self.SaveCurrentProject = QAction(IT['SaveCurrentProject'])
            self.SaveCurrentProject.setObjectName('SaveCurrentProject')
            self.SaveCurrentProject.setIcon(QIcon('Resources/Icons/Save.ico'))
            self.SaveCurrentProject.setShortcut(QKeySequence('CTRL+SHIFT+S'))
            self.SaveCurrentProject.triggered.connect(self.SaveCurrentProject_Triggered)

            self.SaveAllProjects = QAction(IT['SaveAllProjects'])
            self.SaveAllProjects.setObjectName('SaveAllProjects')
            self.SaveAllProjects.setIcon(QIcon('Resources/Icons/Save.ico'))
            self.SaveAllProjects.setShortcut(QKeySequence('SHIFT+S'))
            self.SaveAllProjects.triggered.connect(self.SaveAllProjects_Triggered)

            self.OpenFiles = QAction(IT['OpenFiles'])
            self.OpenFiles.setObjectName('OpenFiles')
            self.OpenFiles.setShortcut(QKeySequence('CTRL+O'))
            self.OpenFiles.triggered.connect(self.OpenFiles_Triggered)

            self.OpenProjects = QAction(IT['OpenProjects'])
            self.OpenProjects.setObjectName('OpenProjects')
            self.OpenProjects.setIcon(QIcon('Resources/Icons/OpenFolder.ico'))
            self.OpenProjects.setShortcut(QKeySequence('CTRL+SHIFT+O'))
            self.OpenProjects.triggered.connect(self.OpenProjects_Triggered)

            self.CloseCurrentFile = QAction(IT['CloseCurrentFile'])
            self.CloseCurrentFile.setObjectName('CloseCurrentFile')
            self.CloseCurrentFile.setIcon(QIcon('Resources/Icons/Close.ico'))
            self.CloseCurrentFile.setShortcut(QKeySequence('CTRL+W'))
            self.CloseCurrentFile.triggered.connect(self.CloseCurrentFile_Triggered)

            self.CloseAllOpenFilesInTheCurrentProject = QAction(IT['CloseAllOpenFilesInTheCurrentProject'])
            self.CloseAllOpenFilesInTheCurrentProject.setObjectName('CloseAllOpenFilesInTheCurrentProject')
            self.CloseAllOpenFilesInTheCurrentProject.setIcon(QIcon('Resources/Icons/Close.ico'))
            self.CloseAllOpenFilesInTheCurrentProject.setShortcut(QKeySequence('CTRL+SHIFT+W'))
            self.CloseAllOpenFilesInTheCurrentProject.triggered.connect(self.CloseAllFilesInTheCurrentProject_Triggered)

            self.CloseCurrentProject = QAction(IT['CloseCurrentProject'])
            self.CloseCurrentProject.setObjectName('CloseCurrentProject')
            self.CloseCurrentProject.setIcon(QIcon('Resources/Icons/Close.ico'))
            self.CloseCurrentProject.setShortcut(QKeySequence('ALT+X'))
            self.CloseCurrentProject.triggered.connect(self.CloseCurrentProject_Triggered)

            self.CloseAllOpenFilesInAllProjects = QAction(IT['CloseAllOpenFilesInAllProjects'])
            self.CloseAllOpenFilesInAllProjects.setObjectName('CloseAllOpenFilesInAllProjects')
            self.CloseAllOpenFilesInAllProjects.setIcon(QIcon('Resources/Icons/Close.ico'))
            self.CloseAllOpenFilesInAllProjects.setShortcut(QKeySequence('CTRL+SHIFT+ALT+X'))
            self.CloseAllOpenFilesInAllProjects.triggered.connect(self.CloseAllFilesInAllProjects_Triggered)

            self.CloseAllProjects = QAction(IT['CloseAllProjects'])
            self.CloseAllProjects.setObjectName('CloseAllProjects')
            self.CloseAllProjects.setIcon(QIcon('Resources/Icons/Close.ico'))
            self.CloseAllProjects.setShortcut(QKeySequence('CTRL+ALT+X'))
            self.CloseAllProjects.triggered.connect(self.CloseAllProjects_Triggered)

            self.Undo = QAction(IT['Undo'])
            self.Undo.setObjectName('Undo')
            self.Undo.setIcon(QIcon('Resources/Icons/Undo.ico'))
            self.Undo.setShortcut(QKeySequence('CTRL+Z'))
            self.Undo.triggered.connect(self.Undo_Triggered)

            self.Redo = QAction(IT['Redo'])
            self.Redo.setObjectName('Redo')
            self.Redo.setIcon(QIcon('Resources/Icons/Redo.ico'))
            self.Redo.setShortcut(QKeySequence('CTRL+Y'))
            self.Redo.triggered.connect(self.Redo_Triggered)

            self.Copy = QAction(IT['Copy'])
            self.Copy.setObjectName('Copy')
            self.Copy.setIcon(QIcon('Resources/Icons/Copy.ico'))
            self.Copy.setShortcut(QKeySequence('CTRL+C'))
            self.Copy.triggered.connect(self.Copy_Triggered)

            self.Paste = QAction(IT['Paste'])
            self.Paste.setObjectName('Paste')
            self.Paste.setIcon(QIcon('Resources/Icons/Paste.ico'))
            self.Paste.setShortcut(QKeySequence('CTRL+V'))
            self.Paste.triggered.connect(self.Paste_Triggered)

            self.Cut = QAction(IT['Cut'])
            self.Cut.setObjectName('Cut')
            self.Cut.setIcon(QIcon('Resources/Icons/Cut.ico'))
            self.Cut.setShortcut(QKeySequence('CTRL+X'))
            self.Cut.triggered.connect(self.Cut_Triggered)

            self.SelectCurrentLine = QAction(IT['SelectCurrentLine'])
            self.SelectCurrentLine.setObjectName('SelectCurrentLine')
            self.SelectCurrentLine.setShortcut(QKeySequence('CTRL+L'))
            self.SelectCurrentLine.triggered.connect(self.SelectCurrentLine_Triggered)

            self.SelectAll = QAction(IT['SelectAll'])
            self.SelectAll.setObjectName('SelectAll')
            self.SelectAll.setIcon(QIcon('Resources/Icons/SelectAll.ico'))
            self.SelectAll.setShortcut(QKeySequence('CTRL+A'))
            self.SelectAll.triggered.connect(self.SelectAll_Triggered)

            self.DuplicateCurrentLine = QAction(IT['DuplicateCurrentLine'])
            self.DuplicateCurrentLine.setObjectName('DuplicateCurrentLine')
            self.DuplicateCurrentLine.setShortcut(QKeySequence('SHIFT+L'))
            self.DuplicateCurrentLine.triggered.connect(self.DuplicateCurrentLine_Triggered)

            self.DuplicateSelection = QAction(IT['DuplicateSelection'])
            self.DuplicateSelection.setObjectName('DuplicateSelection')
            self.DuplicateSelection.setShortcut(QKeySequence('CTRL+D'))
            self.DuplicateSelection.triggered.connect(self.DuplicateSelection_Triggered)

            self.DeleteCurrentLine = QAction(IT['DeleteCurrentLine'])
            self.DeleteCurrentLine.setObjectName('DeleteCurrentLine')
            self.DeleteCurrentLine.setIcon(QIcon('Resources/Icons/Trash.ico'))
            self.DeleteCurrentLine.setShortcut(QKeySequence('CTRL+SHIFT+L'))
            self.DeleteCurrentLine.triggered.connect(self.DeleteCurrentLine_Triggered)

            self.DeleteSelection = QAction(IT['DeleteSelection'])
            self.DeleteSelection.setObjectName('DeleteSelection')
            self.DeleteSelection.setIcon(QIcon('Resources/Icons/Trash.ico'))
            self.DeleteSelection.setShortcut(QKeySequence('CTRL+Delete'))
            self.DeleteSelection.triggered.connect(self.DeleteSelection_Triggered)

            self.IndentCurrentLine = QAction(IT['IndentCurrentLine'])
            self.IndentCurrentLine.setObjectName('IndentCurrentLine')
            self.IndentCurrentLine.setIcon(QIcon('Resources/Icons/Indent.ico'))
            self.IndentCurrentLine.setShortcut(QKeySequence('CTRL+I'))
            self.IndentCurrentLine.triggered.connect(self.IndentCurrentLine_Triggered)

            self.IndentSelection = QAction(IT['IndentSelection'])
            self.IndentSelection.setObjectName('IndentSelection')
            self.IndentSelection.setIcon(QIcon('Resources/Icons/Indent.ico'))
            self.IndentSelection.setShortcut(QKeySequence('CTRL+SHIFT+I'))
            self.IndentSelection.triggered.connect(self.IndentSelection_Triggered)

            self.UnindentCurrentLine = QAction('UnindentCurrentLine')
            self.UnindentCurrentLine.setObjectName('UnindentCurrentLine')
            self.UnindentCurrentLine.setIcon(QIcon('Resources/Icons/Unindent.ico'))
            self.UnindentCurrentLine.setShortcut(QKeySequence('CTRL+U'))
            self.UnindentCurrentLine.triggered.connect(self.UnindentCurrentLine_Triggered)

            self.UnindentSelection = QAction(IT['UnindentSelection'])
            self.UnindentSelection.setObjectName('UnindentSelection')
            self.UnindentSelection.setIcon(QIcon('Resources/Icons/Unindent.ico'))
            self.UnindentSelection.setShortcut(QKeySequence('CTRL+SHIFT+U'))
            self.UnindentSelection.triggered.connect(self.UnindentSelection_Triggered)

            self.ShowProjectTree = QAction(IT['ProjectTree'])
            self.ShowProjectTree.setObjectName('ShowProjectTree')
            self.ShowProjectTree.setIcon(QIcon('Resources/Icons/ProjectTree.ico'))
            self.ShowProjectTree.triggered.connect(self.ShowProjectTree_Triggered)

            self.ShowProjectStructure = QAction(IT['ProjectStructure'])
            self.ShowProjectStructure.setObjectName('ShowProjectStructure')
            self.ShowProjectStructure.setIcon(QIcon('Resources/Icons/ProjectTree.ico'))
            self.ShowProjectStructure.triggered.connect(self.ShowProjectStructure_Triggered)

            self.ShowChat = QAction(IT['Chat'])
            self.ShowChat.setObjectName('ShowChat')
            self.ShowChat.setIcon(QIcon('Resources/Icons/Chat.ico'))
            self.ShowChat.triggered.connect(self.ShowChat_Triggered)

            self.ShowTasks = QAction(IT['Tasks'])
            self.ShowTasks.setObjectName('ShowTasks')
            self.ShowTasks.setIcon(QIcon('Resources/Icons/Tasks.ico'))
            self.ShowTasks.triggered.connect(self.ShowTasks_Triggered)

            self.ShowNotifications = QAction(IT['Notifications'])
            self.ShowNotifications.setObjectName('ShowNotifications')
            self.ShowNotifications.setIcon(QIcon('Resources/Icons/Notifications.ico'))
            self.ShowNotifications.triggered.connect(self.ShowNotifications_Triggered)

            self.ShowOutput = QAction(IT['Output'])
            self.ShowOutput.setObjectName('ShowOutput')
            self.ShowOutput.triggered.connect(self.ShowOutput_Triggered)

            self.ShowConsole = QAction(IT['Console'])
            self.ShowConsole.setObjectName('ShowConsole')
            self.ShowConsole.setIcon(QIcon('Resources/Icons/Console.ico'))
            self.ShowConsole.triggered.connect(self.ShowConsole_Triggered)

            self.ShowGitPanel = QAction(IT['GitPanel'])
            self.ShowGitPanel.setObjectName('ShowGitPanel')
            self.ShowGitPanel.setIcon(QIcon('Resources/Icons/Git.ico'))
            self.ShowGitPanel.triggered.connect(self.ShowGitPanel_Triggered)

            self.ShowProblems = QAction(IT['Problems'])
            self.ShowProblems.setObjectName('ShowProblems')
            self.ShowProblems.setIcon(QIcon('Resources/Icons/Info.ico'))
            self.ShowProblems.triggered.connect(self.ShowProblems_Triggered)

            self.File.addAction(self.NewFile)
            self.File.addAction(self.NewProject)
            self.File.addAction(self.SaveCurrentFile)
            self.File.addAction(self.SaveCurrentProject)
            self.File.addAction(self.SaveAllProjects)
            self.File.addAction(self.OpenFiles)
            self.File.addAction(self.OpenProjects)
            self.File.addAction(self.CloseCurrentFile)
            self.File.addAction(self.CloseAllOpenFilesInTheCurrentProject)
            self.File.addAction(self.CloseCurrentProject)
            self.File.addAction(self.CloseAllOpenFilesInAllProjects)
            self.File.addAction(self.CloseAllProjects)

            self.Edit.addAction(self.Undo)
            self.Edit.addAction(self.Redo)
            self.Edit.addAction(self.Copy)
            self.Edit.addAction(self.Paste)
            self.Edit.addAction(self.Cut)
            self.Edit.addAction(self.SelectCurrentLine)
            self.Edit.addAction(self.SelectAll)
            self.Edit.addAction(self.DuplicateCurrentLine)
            self.Edit.addAction(self.DuplicateSelection)
            self.Edit.addAction(self.DeleteCurrentLine)
            self.Edit.addAction(self.DeleteSelection)
            self.Edit.addAction(self.IndentCurrentLine)
            self.Edit.addAction(self.IndentSelection)
            self.Edit.addAction(self.UnindentCurrentLine)
            self.Edit.addAction(self.UnindentSelection)

            self.View.addAction(self.ShowProjectTree)
            self.View.addAction(self.ShowChat)
            self.View.addAction(self.ShowTasks)
            self.View.addAction(self.ShowNotifications)
            self.View.addAction(self.ShowOutput)
            self.View.addAction(self.ShowConsole)
            self.View.addAction(self.ShowProblems)
            self.View.addAction(self.ShowProjectStructure)
            self.View.addAction(self.ShowGitPanel)

            for Font in self.MenuBar.actions():
                Font.setFont(QFont(InterfaceFont))

            self.setMenuBar(self.MenuBar)

        def NewFile_Triggered(self):
            global NewFiles
            NewFiles += 1

            Widget = QWidget()
            Files.append(Widget)

            self.Files.insertTab(self.Files.count() - 1, Widget, f'New File ({NewFiles})')
            self.Files.setCurrentIndex(self.Files.indexOf(Widget))
            self.Files.move()

        def NewProject_Triggered(self):
            pass

        def SaveCurrentFile_Triggered(self):
            pass

        def SaveCurrentProject_Triggered(self):
            pass

        def SaveAllProjects_Triggered(self):
            pass

        def OpenFiles_Triggered(self):
            pass

        def OpenProjects_Triggered(self):
            pass

        def CloseCurrentFile_Triggered(self):
            pass

        def CloseAllFilesInTheCurrentProject_Triggered(self):
            pass

        def CloseCurrentProject_Triggered(self):
            pass

        def CloseAllFilesInAllProjects_Triggered(self):
            pass

        def CloseAllProjects_Triggered(self):
            pass

        def Undo_Triggered(self):
            pass

        def Redo_Triggered(self):
            pass

        def Cut_Triggered(self):
            self.Files.currentWidget().findChild(QTextEdit, 'Editor').cut()

        def Copy_Triggered(self):
            pass

        def Paste_Triggered(self):
            pass

        def SelectCurrentLine_Triggered(self):
            pass

        def SelectAll_Triggered(self):
            pass

        def DuplicateCurrentLine_Triggered(self):
            pass

        def DuplicateSelection_Triggered(self):
            pass

        def DeleteCurrentLine_Triggered(self):
            pass

        def DeleteSelection_Triggered(self):
            pass

        def IndentCurrentLine_Triggered(self):
            pass

        def IndentSelection_Triggered(self):
            pass

        def UnindentCurrentLine_Triggered(self):
            pass

        def UnindentSelection_Triggered(self):
            pass

        def ShowProjectTree_Triggered(self):
            pass

        def ShowProjectStructure_Triggered(self):
            pass

        def ShowGitPanel_Triggered(self):
            pass

        def ShowChat_Triggered(self):
            pass

        def ShowProblems_Triggered(self):
            pass

        def ShowOutput_Triggered(self):
            pass

        def ShowConsole_Triggered(self):
            pass

        def ShowTasks_Triggered(self):
            pass

        def ShowNotifications_Triggered(self):
            pass

    Aplicacao = QApplication([])
    Window = GoIDE()
    Window.setWindowTitle("GoIDE - No Opened Project/File")
    Window.setWindowIcon(QIcon('Resources/Icons/Gold-Icon-128.ico'))
    Window.setMaximumSize(1920, 1080)
    Window.setMinimumSize(1920, 1080)
    Window.showMaximized()
    sys.exit(Aplicacao.exec())


def ChecarConexaoComAInternet():
    URL = 'https://www.google.com'
    LimiteDeTempo = 10

    try:
        _ = requests.get(URL, timeout=LimiteDeTempo)
        Construir()
    except requests.ConnectionError:
        print('Internet Não Disponível')
    return False


if __name__ == '__main__':
    ChecarConexaoComAInternet()
